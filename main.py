from api_client import APIClient
from serial_listener import SerialListener
from dotenv import load_dotenv
from os import getenv


def main():
    load_dotenv()
    port = getenv("SERIAL_PORT")
    baudrate = getenv("SERIAL_BAUDRATE")
    base_url = getenv("API_BASE_URL")
    client_id = getenv("API_CLIENT_ID")
    client_secret = getenv("API_CLIENT_SECRET")

    api = APIClient(base_url, client_id, client_secret)
    s = SerialListener(port, baudrate)

    for passport_id in s.listen():
        thwin_id = passport_id.split('-')[0]
        api.postUserScanned(thwin_id)


if __name__ == "__main__":
    main()
