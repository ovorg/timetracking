import serial
from time import sleep


class SerialListener():

    def __init__(self, port, baudrate, delay=10):
        """Initialize the SerialListener

        Parameters
        ----------
        port : string
            The port to use for the serial connection.
        baudrate : int
            The baudrate to use for the serial connection.
        delay : int, default=10
            The time to wait after it failed to open a new connection
            in seconds.
        """
        self.port = port
        self.baudrate = baudrate
        self.delay = delay

    def get_connection(self, port, baudrate):
        """Create a serial connection

        Only returns when the connection was successfully established.
        Retry after a delay on SerialException.

        Parameters
        ----------
        port : string
            The port to use for the serial connection.
        baudrate : int
            The baudrate to use for the serial connection.

        Returns
        -------
        serial.Serial
            The serial connection.

        Raises
        ------
        ValueError
            Will be raised when serial.Serial throws a ValueError.
        """
        while 1:
            try:
                conn = serial.Serial(port=port, baudrate=baudrate)
            except serial.SerialException as ex:
                print(ex)
            else:
                return conn

            sleep(self.delay)

    def yield_lines(self, conn):
        """Yield the serial input read from conn using serial.Serial.readline.

        yield_lines wont return.
        \r and \n are stripped from the string.

        Parameters
        ----------
        conn : serial.Serial
            The serial connection.
        """
        while 1:
            yield conn.readline().decode().strip("\r").strip("\n")

    def listen(self):
        """Yield the return values of self.yield_lines().

        Create new connections using self.get_connection().
        listen wont return. It handles serial.SerialException.

        Raises
        ------
        ValueError
            Will be raised when self.get_connection throws a ValueError.
        """
        while 1:
            conn = self.get_connection(self.port, self.baudrate)
            try:
                for s in self.yield_lines(conn):
                    yield s
            except serial.SerialException as ex:
                conn.close()
                print(ex)
