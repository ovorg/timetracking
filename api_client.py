import requests
import json
from time import sleep


class APIClient():

    def __init__(self, base_url, client_id, client_secret):
        """Initialize the API Client

        Parameters
        ----------
        base_url : string
            The base url to use for the api calls. You should use https!
        client_id : int
            The client_id to use to authenticate.
        client_secret : string
            The client_secret to use to authenticate.
        """
        self.base_url = base_url
        self.client_id = client_id
        self.client_secret = client_secret
        self._login()

    def _get_headers(self):
        if self.access_token:
            return {
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Bearer " + self.access_token
            }
        else:
            return {
                "Content-Type": "application/json",
                "Accept": "application/json",
            }

    def _login(self):
        self.access_token = None
        data = {
            "client_id": self.client_id,
            "client_secret": self.client_secret,
            "grant_type": "client_credentials",
            "scope": ["time_tracking"]
        }

        r = requests.post(self.base_url + "/oauth/token",
                          data=json.dumps(data), headers=self._get_headers())

        if r.status_code is 200:
            self.access_token = json.loads(r.text)["access_token"]
        else:  # TODO: Error handling
            raise Exception("Login failed.")

    def postUserScanned(self, thwin_id):
        """Post that a user has been scanned.

        Parameters
        ----------
        thwin_id : string
            The value to submit for thwin_id.
        """
        data = {
            "thwin_id": thwin_id
        }

        def send():
            while 1:
                try:
                    return requests.patch(self.base_url + "/api/time_tracking/scanned", data=json.dumps(data), headers=self._get_headers())
                except requests.exceptions.ConnectionError:
                    print("ConnectionError. Retrying...")
                    sleep(5)

        r = send()
        if r.status_code == 401:
            self._login()
            r = send()

        if r.status_code != 200:
            print("UserScanned failed. Status = %d, Message = %s" %
                  (r.status_code, r.text))
